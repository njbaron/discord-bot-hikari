""" Misc commands module. """
import random

import tanjun

component = tanjun.Component()


@component.with_slash_command
@tanjun.with_str_slash_option("question", "Ask a question to the magic 8 ball.")
@tanjun.as_slash_command(
    "8ball",
    "Ask the magic 8 ball a question and receive an undisputable answer.")
async def command_8ball(ctx: tanjun.abc.Context, question: str) -> None:
    """8 ball question slash command.

    Args:
        ctx (tanjun.abc.Context): The context passed in by tanjun.
        question (str): The question asked by the user.
    """
    responses = ['It is certain',
                 'Without a doubt',
                 'You may rely on it',
                 'Yes definitely',
                 'It is decidedly so',
                 'As I see it, yes',
                 'Most likely',
                 'Yes',
                 'Outlook good',
                 'Signs point to yes',
                 'Reply hazy try again',
                 'Better not tell you now',
                 'Ask again later',
                 'Cannot predict now',
                 'Concentrate and ask again',
                 'Don’t count on it',
                 'Outlook not so good',
                 'My sources say no',
                 'Very doubtful',
                 'My reply is no']
    await ctx.respond(f'Question: {question}\n`Answer: {random.choice(responses)}`')


@component.with_slash_command
@tanjun.with_int_slash_option("number", "The number of dice that you want to roll.")
@tanjun.with_int_slash_option("sides", "The number of sides on the dice.", default=6)
@tanjun.with_int_slash_option("bonus", "The bonus to add to the dice roll total.", default=0)
@tanjun.as_slash_command("dice", "Roll a set of dice dnd style.")
async def command_dice(ctx: tanjun.abc.Context, number: int, sides: int, bonus: int):
    """Dice command that emulated rolling dice in dnd.

    Args:
        ctx (tanjun.abc.Context): The context passed in by tanjun.
        number (int): The number of dice to roll.
        sides (int): The number of sides of the dice.
        bonus (int): The bonus value added after rolling dice.
    """
    if number > 25:
        ctx.respond("Cannot roll more than 25 dice.")
        return

    if sides > 100:
        ctx.respond("Cannot roll a dice with more than 100 sides.")

    values = []
    total = bonus
    for _ in range(number):
        val = random.randint(1, sides)
        total += val
        values.append(val)

    response = " + ".join(str(value) for value in values)
    if bonus > 0:
        response += f" + {bonus}(bonus value)"
    response += f" = {total}"

    await ctx.respond(f'`{response}`')


@tanjun.as_loader
def load_component(client: tanjun.abc.Client) -> None:
    """Generic load function used by tanjun to load this module.

    Args:
        client (tanjun.abc.Client): A referece to the tanjun client.
    """
    client.add_component(component.copy())
