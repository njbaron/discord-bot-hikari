""" Config Module. A central place to read environment variables for the bot."""
import os

from dotenv import load_dotenv

load_dotenv()  # take environment variables from .env.

TOKEN = os.getenv('TOKEN', "")
GUILD_ID = os.getenv('GUILD_ID', "")
