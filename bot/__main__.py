""" The main module enrty point. Builds and runs the bot instance."""

import os
from pathlib import Path

import hikari
import tanjun

from . import config

if os.name != "nt":
    import uvloop
    uvloop.install()


def create_bot() -> hikari.GatewayBot:
    """Creates an instance of the bot and converts it to a tanjun instance.

    Returns:
        hikari.GatewayBot: A reference to the hikari gateway bot.
    """
    bot = hikari.GatewayBot(token=config.TOKEN)
    client = tanjun.Client.from_gateway_bot(
        bot, set_global_commands=hikari.Snowflake(config.GUILD_ID))
    client.load_modules(*Path("./bot/modules").glob("mod_*.py"))
    return bot


if __name__ == "__main__":
    create_bot().run()
