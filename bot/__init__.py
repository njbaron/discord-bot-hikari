"""Bot Init"""
from datetime import date

__version__ = "dev_" + date.today().strftime('%Y.%m.%d')
